from django.test import TestCase, Client, LiveServerTestCase
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

import os
import time


# Create your tests here.

class UnitTest(TestCase):
    def test_landing_page_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_landing_page_html(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'indexProfilku.html')


class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        super(FunctionalTest, self).setUp()

        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument("--headless")
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('disable-gpu')

        if 'GITLAB_CI' in os.environ:
            self.browser = webdriver.Chrome(
                './chromedriver', chrome_options=chrome_options)
        else:
            self.browser = webdriver.Chrome(chrome_options=chrome_options)        

    def tearDown(self):
        self.browser.quit()

        super(FunctionalTest, self).tearDown()

    def test_accordion_show_data_when_clicked(self):
        acitvity_keyword = ['Belajar Progamming', 'Nonton La Casa de Papel', 'Kuliah Online']
        organizational_experience_keyword = ['COMPFEST 12', 'SISTEM', 'OSIS', 'Sky Avenue']
        achievement_keyword = ['Indonesian Robotic Olympiad']
        contact_me_keyword = ['thariq.razan@ui.ac.id']

        self.browser.get(self.live_server_url + '/')
        time.sleep(10)
        self.browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")

        activity_button = self.browser.find_element_by_xpath("//div[@class='accordion-header-left'][contains(.,'Aktivitas Sekarang')][1]")
        activity_button.click()

        xpath_statement = "//div[@class='accordion-body']"

        for keyword in acitvity_keyword:
            xpath_statement += "[contains(.,'"+keyword+"')]"

        xpath_statement += "[1]"

        activity_body = self.browser.find_element_by_xpath(xpath_statement)
        self.assertTrue(activity_body.is_displayed())

        organizational_button = self.browser.find_element_by_xpath("//div[@class='accordion-header-left'][contains(.,'Pengalaman Organisasi/Kepanitiaan')][1]")
        organizational_button.click()
        time.sleep(10)

        xpath_statement = "//div[@class='accordion-body']"

        for keyword in organizational_experience_keyword:
            xpath_statement += "[contains(.,'"+keyword+"')]"

        xpath_statement += "[1]"

        organizational_body = self.browser.find_element_by_xpath(xpath_statement)
        self.assertTrue(organizational_body.is_displayed())

        achievement_button = self.browser.find_element_by_xpath("//div[@class='accordion-header-left'][contains(.,'Prestasi')][1]")
        achievement_button.click()
        time.sleep(10)

        xpath_statement = "//div[@class='accordion-body']"

        for keyword in achievement_keyword:
            xpath_statement += "[contains(.,'"+keyword+"')]"

        xpath_statement += "[1]"

        achievement_body = self.browser.find_element_by_xpath(xpath_statement)
        self.assertTrue(achievement_body.is_displayed())

        achievement_button = self.browser.find_element_by_xpath("//div[@class='accordion-header-left'][contains(.,'Kontak')][1]")
        achievement_button.click()
        time.sleep(10)

        xpath_statement = "//div[@class='accordion-body']"

        for keyword in contact_me_keyword:
            xpath_statement += "[contains(.,'"+keyword+"')]"

        xpath_statement += "[1]"

        contact_me_body = self.browser.find_element_by_xpath(xpath_statement)
        self.assertTrue(contact_me_body.is_displayed())

    def test_accordion_movement(self):
        self.browser.get(self.live_server_url + '/')
        
        down_activity_button = self.browser.find_element_by_xpath("//div[@class='accordion shadow aos-init aos-animate'][contains(.,'Aktivitas Sekarang')]/div[1]/div[2]/div[2]")

        page_source = self.browser.page_source

        self.assertTrue(page_source.find('Aktivitas Sekarang') < page_source.find('Pengalaman Organisasi/Kepanitiaan'))

        down_activity_button.click()
        page_source = self.browser.page_source

        self.assertTrue(page_source.find('Aktivitas Sekarang') > page_source.find('Pengalaman Organisasi/Kepanitiaan'))

        down_organizational_button = self.browser.find_element_by_xpath("//div[@class='accordion shadow aos-init aos-animate'][contains(.,'Pengalaman Organisasi/Kepanitiaan')]/div[1]/div[2]/div[2]")

        down_organizational_button.click()
        page_source = self.browser.page_source

        self.assertTrue(page_source.find('Aktivitas Sekarang') < page_source.find('Pengalaman Organisasi/Kepanitiaan'))

    def test_theme_change(self):
        self.browser.get(self.live_server_url + '/')

        # body_color_before = self.browser.find_element_by_css_selector("body")
        accordion_before = self.browser.find_element_by_xpath("//div[@class='accordion-header'][1]")
        accordion_before_background_color = accordion_before.value_of_css_property("background-color")
        accordion_before_color = accordion_before.value_of_css_property("color")

        change_button = self.browser.find_element_by_id("background-btn")
        change_button.click()
        time.sleep(10)

        # body_color_after = self.browser.find_element_by_css_selector("body")
        accordion_after = self.browser.find_element_by_xpath("//div[@class='accordion-header'][1]")
        accordion_after_background_color = accordion_after.value_of_css_property("background-color")
        accordion_after_color = accordion_after.value_of_css_property("color")

        # self.assertNotEqual(body_color_before, body_color_after)
        self.assertNotEqual(accordion_before_background_color, accordion_after_background_color)
        self.assertNotEqual(accordion_before_color, accordion_after_color)
